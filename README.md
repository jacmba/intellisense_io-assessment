# IntelliSense API assessment

This microservice exposes an endpoint to get reduced data by given period.

## Setup and execution

In order to execute this service, please follow these steps:

1. Create custom docker compose file from provided sample. E.g:

```
cp docker-compose.sample.yml docker-compose.yml
```

2. Modify exposed port, cache expiration time and data provider endpoint URI if needed.

3. Launch the service with the following command (image will be built the first time):

```
docker-compose up -d
```

4. Test the endpoint by calling the root path

```
curl --location --request POST 'localhost:3000' \
--header 'Content-Type: application/json' \
--data-raw '{
    "period": 10
}'
```

## Run the tests

You may also want to run the code tests. To accomplish this, you can take 3 different main actions:

* Run **Unit tests**

```
npm test
```

* Run **Coverage report**

```
npm run test:cov
```

* Run **e2e tests**

```
npm run test:e2e
```

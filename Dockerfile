FROM node:14-alpine
RUN mkdir /src
WORKDIR /src

COPY . .

RUN npm i
RUN npm run build

RUN mv dist /app
RUN mv node_modules /app/

WORKDIR /app
RUN rm -rf /src

CMD ["node", "main.js"]
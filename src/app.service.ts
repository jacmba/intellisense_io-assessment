import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { CacheProvider } from './cache-provider';
import { DataProvider } from './data-provider';
import { Reducer } from './reducer';

/**
 * Domain logic service
 */
@Injectable()
export class AppService {
  constructor(
    private readonly cache: CacheProvider,
    private readonly dataProvider: DataProvider,
    private readonly reducer: Reducer,
  ) {}

  /**
   * Get data from provider and apply average reduction
   * @param period Time frame in minutes for data reduction
   * @returns Reduced data
   */
  async getData(period: number): Promise<object> {
    let data: object;

    try {
      data = await this.cache.getData();
    } catch (e) {
      // Cache failure
      try {
        data = await this.dataProvider.getData();
      } catch (e) {
        // Provider request failure
        throw new HttpException(
          `Error getting data from online provider: ${e.msg || e.message || e}`,
          HttpStatus.SERVICE_UNAVAILABLE,
        );
      }
    }

    return this.reducer.reduce(data, period);
  }
}

import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import ApiRequestDto from './dto/apiRequest.dto';
import { HttpException, HttpStatus } from '@nestjs/common';
import exp from 'constants';

describe('AppController', () => {
  let appController: AppController;

  const mockService = {
    getData: jest.fn(),
  };

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [
        {
          provide: AppService,
          useValue: mockService,
        },
      ],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  it('Should be defined', () => {
    expect(appController).toBeDefined();
  });

  it('Should get data from domain service', async () => {
    mockService.getData.mockResolvedValue({ foo: 'bar' });

    const body: ApiRequestDto = {
      period: 10,
    };
    const data = await appController.returnData(body);

    expect(mockService.getData).toHaveBeenCalledWith(10);
    expect(data).toStrictEqual({ foo: 'bar' });
  });

  it('Should propagate 503 http error from service', async () => {
    mockService.getData.mockRejectedValue(
      new HttpException('Service down', HttpStatus.SERVICE_UNAVAILABLE),
    );

    const body: ApiRequestDto = {
      period: 30,
    };

    expect.assertions(3);

    try {
      await appController.returnData(body);
    } catch (e) {
      expect(e).toBeInstanceOf(HttpException);
      const err = e as HttpException;
      expect(err.message).toBe('Service down');
      expect(err.getStatus()).toBe(503);
    }
  });

  it('Should throw an HTTP validation error when sending a non valid period', async () => {
    const body: ApiRequestDto = {
      period: 20,
    };

    expect.assertions(3);

    try {
      await appController.returnData(body);
    } catch (e) {
      expect(e).toBeInstanceOf(HttpException);
      const err = e as HttpException;
      expect(err.message).toBe('Period 20 is not valid. Use 10, 30 or 60');
      expect(err.getStatus()).toBe(400);
    }
  });
});

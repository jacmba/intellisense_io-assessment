import { Injectable } from '@nestjs/common';
import axios from 'axios';
import Config from './config';
import DataProviderError from './error/data-provider.error';

/**
 * This class is responsible for getting data from online provider
 */
@Injectable()
export class DataProvider {
  async getData(): Promise<object> {
    try {
      const response = await axios.get(Config.DATA_URI);

      if (!response || !response.data) {
        throw new Error('Invalid result');
      }

      return response.data;
    } catch (e) {
      throw new DataProviderError(e.msg || e.message || e);
    }
  }
}

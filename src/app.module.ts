import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DataProvider } from './data-provider';
import { CacheProvider } from './cache-provider';
import { Reducer } from './reducer';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, DataProvider, CacheProvider, Reducer],
})
export class AppModule {}

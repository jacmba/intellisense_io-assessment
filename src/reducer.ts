import { Injectable } from '@nestjs/common';

/**
 * Reduces and calculages average of data sets
 */
@Injectable()
export class Reducer {
  /**
   * @param numbers List of numbers
   * @returns Average of provided numbers list
   */
  average(numbers: number[]) {
    return numbers.reduce((a, b) => a + b) / numbers.length;
  }

  /**
   * @param data Data set obtained from provider
   * @param period Period in minutes to execute average
   * @returns Reduced data set averaged by period
   */
  reduce(data: object, period: number): object {
    const result = {};

    Object.keys(data).forEach((assetId) => {
      const asset = {
        time: [],
      };

      // Get averages by metric IDs
      Object.keys(data[assetId])
        .filter((k) => k !== 'time')
        .forEach((metricId) => {
          let list = [];

          for (let i = 0; i < data[assetId][metricId].length; i += period) {
            const numbers = data[assetId][metricId].slice(i, i + period);
            const avg = this.average(numbers);
            list = [...list, avg];
          }

          asset[metricId] = list;
        });

      // Reduce time list
      for (let i = 0; i < data[assetId].time.length; i += period) {
        asset.time = [...asset.time, data[assetId].time[i]];
      }

      result[assetId] = asset;
    });

    return result;
  }
}

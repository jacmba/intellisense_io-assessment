import {
  Body,
  Controller,
  Post,
  HttpException,
  HttpStatus,
  HttpCode,
} from '@nestjs/common';
import { AppService } from './app.service';
import ApiRequestDto from './dto/apiRequest.dto';

const VALID_PERIODS = [10, 30, 60];

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post()
  @HttpCode(200)
  async returnData(@Body() body: ApiRequestDto) {
    if (!VALID_PERIODS.includes(body.period)) {
      throw new HttpException(
        `Period ${body.period} is not valid. Use 10, 30 or 60`,
        HttpStatus.BAD_REQUEST,
      );
    }
    return this.appService.getData(body.period);
  }
}

/**
 * Error thrown when cache is expired or never set
 */
export default class CacheExpiredError extends Error {
  constructor() {
    super('Cache expired or unset');
  }
}

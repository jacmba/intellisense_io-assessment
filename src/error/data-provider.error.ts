/**
 * Error thrown whenever there is an issue getting data from online provider
 */
export default class DataProviderError extends Error {
  constructor(msg: string) {
    super('Error getting data from provider: ' + msg);
  }
}

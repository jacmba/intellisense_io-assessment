import { HttpException, HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import axios from 'axios';
import { DataProvider } from './data-provider';
import DataProviderError from './error/data-provider.error';

jest.mock('axios');

describe('DataProvider', () => {
  let provider: DataProvider;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DataProvider],
    }).compile();

    provider = module.get<DataProvider>(DataProvider);
  });

  it('should be defined', () => {
    expect(provider).toBeDefined();
  });

  it('Should return a mock data set', async () => {
    (axios.get as jest.Mock).mockResolvedValue({
      data: {
        '1234': {
          '5678': [1.0],
          time: ['2022-02-05T10:00:00.000Z'],
        },
      },
    });

    const result = await provider.getData();
    expect(axios.get).toHaveBeenCalled();
    expect(result).toStrictEqual({
      '1234': {
        '5678': [1.0],
        time: ['2022-02-05T10:00:00.000Z'],
      },
    });
  });

  it('Dataprovider exception should be thrown on http get error', async () => {
    (axios.get as jest.Mock).mockRejectedValue(
      new HttpException('Bad request', HttpStatus.BAD_REQUEST),
    );

    expect.assertions(2);
    try {
      await provider.getData();
    } catch (e) {
      expect(e).toBeInstanceOf(DataProviderError);
      expect(e.message).toBe('Error getting data from provider: Bad request');
    }
  });

  it('Dataprovider exception should be thrown when invalid data is returned', async () => {
    (axios.get as jest.Mock).mockResolvedValue({ data: null });

    expect.assertions(2);
    try {
      await provider.getData();
    } catch (e) {
      expect(e).toBeInstanceOf(DataProviderError);
      expect(e.message).toBe(
        'Error getting data from provider: Invalid result',
      );
    }
  });
});

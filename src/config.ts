/**
 * Class with static attributes for configuration values
 */
export default class Config {
  static readonly DATA_URI =
    process.env.DATA_URI ||
    'https://reference.intellisense.io/test.dataprovider';

  static readonly CACHE_EXPIRATION = Number(
    process.env.CACHE_EXPIRATION || 60000,
  );
}

import { Injectable } from '@nestjs/common';
import CacheExpiredError from './error/cache-expired.error';
import Config from './config';

/**
 * Handles in-memory data cache
 */
@Injectable()
export class CacheProvider {
  private data: object;
  private expiration: number;

  constructor() {
    this.data = null;
    this.expiration = 0;
  }

  /**
   * @returns Cache data object
   * @throws  CacheExpiredError when unset
   */
  async getData(): Promise<object> {
    const now = new Date().getTime();
    if (now > this.expiration) {
      this.data = null;
      throw new CacheExpiredError();
    }
    return this.data;
  }

  /**
   * @param data Cache data object
   * @param expiration Optional - expiration time
   */
  setData(data: object, expiration?: number) {
    this.data = data;
    this.expiration =
      expiration || new Date().getTime() + Config.CACHE_EXPIRATION;
  }
}

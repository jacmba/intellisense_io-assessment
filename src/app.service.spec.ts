import { AppService } from './app.service';
import { TestingModule, Test } from '@nestjs/testing';
import { CacheProvider } from './cache-provider';
import { DataProvider } from './data-provider';
import { Reducer } from './reducer';
import CacheExpiredError from './error/cache-expired.error';
import { HttpException, HttpStatus } from '@nestjs/common';

describe('AppService', () => {
  let appService: AppService;

  const mockCache = {
    setData: jest.fn(),
    getData: jest.fn(),
  };

  const mockProvider = {
    getData: jest.fn(),
  };

  const mockReducer = {
    reduce: jest.fn(),
  };

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: CacheProvider,
          useValue: mockCache,
        },
        {
          provide: DataProvider,
          useValue: mockProvider,
        },
        {
          provide: Reducer,
          useValue: mockReducer,
        },
        AppService,
      ],
    }).compile();

    appService = app.get<AppService>(AppService);
  });

  it('Should be defined', () => {
    expect(appService).toBeDefined();
  });

  it('Should return data set from cache', async () => {
    mockCache.getData.mockReturnValue({ foo: 'bar' });
    mockReducer.reduce.mockReturnValue({ lorem: 'ipsum' });

    const data = await appService.getData(10);
    expect(data).toStrictEqual({ lorem: 'ipsum' });
    expect(mockCache.getData).toHaveBeenCalled();
    expect(mockProvider.getData).toHaveBeenCalledTimes(0);
    expect(mockReducer.reduce).toHaveBeenCalledWith({ foo: 'bar' }, 10);
  });

  it('Should return data from online provider when cache fails', async () => {
    mockCache.getData.mockRejectedValue(new CacheExpiredError());
    mockProvider.getData.mockReturnValue({ foo: 'bar' });
    mockReducer.reduce.mockReturnValue({ lorem: 'ipsum' });

    const data = await appService.getData(10);
    expect(data).toStrictEqual({ lorem: 'ipsum' });
    expect(mockCache.getData).toHaveBeenCalled();
    expect(mockProvider.getData).toHaveBeenCalled();
    expect(mockReducer.reduce).toHaveBeenCalledWith({ foo: 'bar' }, 10);
  });

  it('Should throw 503 http error when any issue calling data provider', async () => {
    mockCache.getData.mockRejectedValue(new CacheExpiredError());
    mockProvider.getData.mockRejectedValue(
      new HttpException('We have a problem', HttpStatus.INTERNAL_SERVER_ERROR),
    );

    expect.assertions(3);
    try {
      await appService.getData(10);
    } catch (e) {
      expect(e).toBeInstanceOf(HttpException);
      const err = e as HttpException;
      expect(err.message).toBe(
        'Error getting data from online provider: We have a problem',
      );
      expect(err.getStatus()).toBe(503);
    }
  });
});

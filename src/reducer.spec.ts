import { Test, TestingModule } from '@nestjs/testing';
import { Reducer } from './reducer';

describe('Reducer', () => {
  let provider: Reducer;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [Reducer],
    }).compile();

    provider = module.get<Reducer>(Reducer);
  });

  it('should be defined', () => {
    expect(provider).toBeDefined();
  });

  it('Should calculate the average of a set of numbers', () => {
    const numbers = [4, 4, 8, 8];
    const avg = provider.average(numbers);
    expect(avg).toBe(6);
  });

  it('Should reduce the data set', () => {
    const data = {
      660: {
        123: [4, 4, 4, 4, 4, 8, 8, 8, 8, 8],
        456: [1, 1, 1, 1, 1, 3, 3, 3, 3, 3],
        time: [
          '2020-02-06T12:00:00.000Z',
          '2020-02-06T12:01:00.000Z',
          '2020-02-06T12:02:00.000Z',
          '2020-02-06T12:03:00.000Z',
          '2020-02-06T12:04:00.000Z',
          '2020-02-06T12:05:00.000Z',
          '2020-02-06T12:06:00.000Z',
          '2020-02-06T12:07:00.000Z',
          '2020-02-06T12:08:00.000Z',
          '2020-02-06T12:09:00.000Z',
        ],
      },
    };

    const result = provider.reduce(data, 10);
    expect(result).toStrictEqual({
      660: {
        123: [6],
        456: [2],
        time: ['2020-02-06T12:00:00.000Z'],
      },
    });
  });

  it('Should reduce the data set with less numbers', () => {
    const data = {
      660: {
        123: [4, 4, 4, 4, 4, 8, 8, 8, 10],
        456: [1, 1, 1, 1, 1, 3, 3, 3, 4],
        time: [
          '2020-02-06T12:00:00.000Z',
          '2020-02-06T12:01:00.000Z',
          '2020-02-06T12:02:00.000Z',
          '2020-02-06T12:03:00.000Z',
          '2020-02-06T12:04:00.000Z',
          '2020-02-06T12:05:00.000Z',
          '2020-02-06T12:06:00.000Z',
          '2020-02-06T12:07:00.000Z',
          '2020-02-06T12:08:00.000Z',
        ],
      },
    };

    const result = provider.reduce(data, 10);
    expect(result).toStrictEqual({
      660: {
        123: [6],
        456: [2],
        time: ['2020-02-06T12:00:00.000Z'],
      },
    });
  });

  it('Should reduce the data set with more numbers', () => {
    const data = {
      660: {
        123: [4, 4, 4, 4, 4, 8, 8, 8, 8, 8, 5],
        456: [1, 1, 1, 1, 1, 3, 3, 3, 3, 3, 7],
        time: [
          '2020-02-06T12:00:00.000Z',
          '2020-02-06T12:01:00.000Z',
          '2020-02-06T12:02:00.000Z',
          '2020-02-06T12:03:00.000Z',
          '2020-02-06T12:04:00.000Z',
          '2020-02-06T12:05:00.000Z',
          '2020-02-06T12:06:00.000Z',
          '2020-02-06T12:07:00.000Z',
          '2020-02-06T12:08:00.000Z',
          '2020-02-06T12:09:00.000Z',
          '2020-02-06T12:10:00.000Z',
        ],
      },
    };

    const result = provider.reduce(data, 10);
    expect(result).toStrictEqual({
      660: {
        123: [6, 5],
        456: [2, 7],
        time: ['2020-02-06T12:00:00.000Z', '2020-02-06T12:10:00.000Z'],
      },
    });
  });
});

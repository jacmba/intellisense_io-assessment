import { Test, TestingModule } from '@nestjs/testing';
import { CacheProvider } from './cache-provider';
import CacheExpiredError from './error/cache-expired.error';

describe('CacheProvider', () => {
  let provider: CacheProvider;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CacheProvider],
    }).compile();

    provider = module.get<CacheProvider>(CacheProvider);
  });

  it('should be defined', () => {
    expect(provider).toBeDefined();
  });

  it('Should get a CacheExpiredError when nothing set', async () => {
    expect.assertions(1);
    try {
      await provider.getData();
    } catch (e) {
      expect(e).toBeInstanceOf(CacheExpiredError);
    }
  });

  it('Should return cache object after setting it', async () => {
    provider.setData({ foo: 'bar' }, 999999999999999);
    const data = await provider.getData();
    expect(data).toStrictEqual({ foo: 'bar' });
  });

  it('Should return data with default cache expiration (+1m)', async () => {
    provider.setData({ lorem: 'ipsum' });
    const data = await provider.getData();
    expect(data).toStrictEqual({ lorem: 'ipsum' });
  });

  it('Should throw CacheExpiredError when setting low expiration value', async () => {
    provider.setData({ key: 'value' }, 100);
    expect.assertions(1);
    try {
      await provider.getData();
    } catch (e) {
      expect(e).toBeInstanceOf(CacheExpiredError);
    }
  });
});
